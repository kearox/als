#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "Library/CharacterEnumLibrary.h"
#include "AnimNotifyGroundedEntryState.generated.h"

UCLASS()
class ALS_API UAnimNotifyGroundedEntryState : public UAnimNotify
{
	GENERATED_BODY()

		virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	virtual FString GetNotifyName_Implementation() const override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AnimNotify)
		EGroundedEntryState GroundedEntryState = EGroundedEntryState::None;
};
