#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShakeBase.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "AnimNotifyCameraShake.generated.h"

class UCameraShakeBase;

UCLASS()
class ALS_API UAnimNotifyCameraShake : public UAnimNotify
{
	GENERATED_BODY()

		virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AnimNotify)
		TSubclassOf<UCameraShakeBase> ShakeClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AnimNotify)
		float Scale = 1.0f;
};