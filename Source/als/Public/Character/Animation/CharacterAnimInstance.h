#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Library/AnimationStructLibrary.h"
#include "Library/StructEnumLibrary.h"
#include "CharacterAnimInstance.generated.h"

// forward declarations
class ABaseCharacter;
class UCurveFloat;
class UAnimSequence;
class UCurveVector;

UCLASS(Blueprintable, BlueprintType)
class ALS_API UCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeInitializeAnimation() override;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;


	/** Return mutable reference of character information to edit them easily inside character class */
	FAnimCharacterInformation& GetCharacterInformationMutable()
	{
		return CharacterInformation;
	}

protected:
	/** References */
	UPROPERTY(BlueprintReadOnly, Category = "Read Only Data|Character Information")
		ABaseCharacter* Character = nullptr;

	/** Character Information */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Read Only Data|Character Information", Meta = (
		ShowOnlyInnerProperties))
		FAnimCharacterInformation CharacterInformation;

public:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		FMovementState MovementState = EMovementState::None;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		FMovementAction MovementAction = EMovementAction::None;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		FRotationMode RotationMode = ERotationMode::LookingDirection;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		FGait Gait = EGait::Walking;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		FStance Stance = EStance::Standing;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		FOverlayState OverlayState = EOverlayState::Default;




	//Notify
	UFUNCTION(BlueprintCallable, Category = "Grounded")
		void SetOverlayOverrideState(int32 OverlayOverrideStates)
	{
		OverlayOverrideState = OverlayOverrideStates;
	}

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Anim Graph - Layer Blending")
		int32 OverlayOverrideState = 0;

	UFUNCTION(BlueprintCallable, Category = "Grounded")
		void SetGroundedEntryState(EGroundedEntryState NewGroundedEntryState)
	{
		GroundedEntryState = NewGroundedEntryState;
	}

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Anim Graph - Grounded")
		FGroundedEntryState GroundedEntryState = EGroundedEntryState::None;
};
