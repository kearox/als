#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Library/CharacterEnumLibrary.h"
#include "Library/CharacterStructLibrary.h"
#include "PlayerCameraBehavior.generated.h"

class ABasePlayerController;

UCLASS(Blueprintable, BlueprintType)
class ALS_API UPlayerCameraBehavior : public UAnimInstance
{
	GENERATED_BODY()

public:
	void SetRotationMode(ERotationMode RotationModes);

	UFUNCTION(BlueprintCallable, Category = "Camera System")
		void SetRightShoulder(bool bNewRightShoulder);

	UFUNCTION(BlueprintGetter, Category = "Camera System")
		bool IsRightShoulder() const { return bRightShoulder; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		EMovementState MovementState;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		EMovementAction MovementAction;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		bool bLookingDirection = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		bool bVelocityDirection = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		bool bAiming = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		ERotationMode RotationMode;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		EGait Gait;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		EStance Stance;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		EViewMode ViewMode;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Read Only Data|Character Information")
		bool bRightShoulder = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Read Only Data|Character Information")
		bool bDebugView = false;

	UPROPERTY(BlueprintReadWrite, Category = "Read Only Data|Character Information")
		APlayerController* PlayerController = nullptr;

	UPROPERTY(BlueprintReadWrite, Category = "Read Only Data|Character Information")
		APawn* ControlledPawn = nullptr;

	//Camera Model

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera System")
		FDataTableRowHandle CameraModel;
	UPROPERTY(BlueprintReadOnly, Category = "Camera System")
		FCameraStateSettings CameraData;

protected:

	virtual void NativeInitializeAnimation() override;

	void SetCameraModel();

};
