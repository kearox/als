#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BaseCharacter.h"
#include "BasePlayerController.generated.h"

class ABaseCharacter;

UCLASS()
class ALS_API ABasePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void OnPossess(APawn* NewPawn) override;

	virtual void BeginPlayingState() override;

	//Debug
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bShowHud = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bDebugView = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bShowTraces = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bShowDebugShapes = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bShowLayerColors = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bShowCharacterInfo = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bSlomo = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bDebugMeshVisible = false;



	UFUNCTION(BlueprintCallable, Category = "Debug")
		void ToggleHud() {};

	UFUNCTION(BlueprintCallable, Category = "Debug")
		void ToggleDebugView() {};

	UFUNCTION(BlueprintCallable, Category = "Debug")
		void ToggleTraces() {};

	UFUNCTION(BlueprintCallable, Category = "Debug")
		void ToggleDebugShapes() {};

	UFUNCTION(BlueprintCallable, Category = "Debug")
		void ToggleLayerColors() {};

	UFUNCTION(BlueprintCallable, Category = "Debug")
		void ToggleCharacterInfo() {};

	UFUNCTION(BlueprintCallable, Category = "Debug")
		void ToggleSlomo() {};

	UFUNCTION(BlueprintCallable, Category = "Debug")
		void ToggleDebugMesh() {};

	UFUNCTION(BlueprintCallable, Category = "Debug")
		bool GetDebugView() { return bDebugView; }

	UFUNCTION(BlueprintCallable, Category = "Debug")
		bool GetShowTraces() { return bShowTraces; }

	UFUNCTION(BlueprintCallable, Category = "Debug")
		bool GetShowDebugShapes() { return bShowDebugShapes; }

	UFUNCTION(BlueprintCallable, Category = "Debug")
		bool GetShowLayerColors() { return bShowLayerColors; }
private:

	void SetupDebugInputs();

public:
	/** Main character reference */
	UPROPERTY(BlueprintReadOnly, Category = "Reference")
		ABaseCharacter* PossessedCharacter = nullptr;
};
