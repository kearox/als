#pragma once

#include "CoreMinimal.h"
#include "Character/BasePlayerCharacter.h"
#include "Components/ActorComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Library/CharacterEnumLibrary.h"
#include "MantleComponents.generated.h"

// forward declarations
class UDebugComponent;
class ABasePlayerController;

UCLASS(Blueprintable, BlueprintType)
class ALS_API UMantleComponents : public UActorComponent
{
	GENERATED_BODY()

public:
	UMantleComponents();

	UFUNCTION(BlueprintCallable, Category = "Mantle System")
		bool MantleCheck(const FMantleTraceSettings& TraceSettings,
			EDrawDebugTrace::Type DebugType);

	UFUNCTION(BlueprintCallable, Category = "Mantle System")
		void MantleStart(float MantleHeight, const FComponentAndTransform& MantleLedgeWS,
			EMantleType MantleType);

	UFUNCTION(BlueprintCallable, Category = "Mantle System")
		void MantleUpdate(float BlendIn);

	UFUNCTION(BlueprintCallable, Category = "Mantle System")
		void MantleEnd();

	UFUNCTION(BlueprintCallable, Category = "Mantle System")
		void OnOwnerJumpInput();

	UFUNCTION(BlueprintCallable, Category = "Mantle System")
		void OnOwnerRagdollStateChanged(bool bRagdollState);

	/** Implement on BP to get correct mantle parameter set according to character state */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Mantle System")
		FMantleAsset GetMantleAsset(EMantleType MantleType, EOverlayState CurrentOverlayState);

protected:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
		FActorComponentTickFunction* ThisTickFunction) override;

	// Called when the game starts
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mantle System")
		UTimelineComponent* MantleTimeline = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Mantle System")
		FMantleTraceSettings GroundedTraceSettings;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Mantle System")
		FMantleTraceSettings AutomaticTraceSettings;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Mantle System")
		FMantleTraceSettings FallingTraceSettings;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Mantle System")
		UCurveFloat* MantleTimelineCurve;

	static FName NAME_IgnoreOnlyPawn;
	/** Profile to use to detect objects we allow mantling */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Mantle System")
		FName MantleObjectDetectionProfile = NAME_IgnoreOnlyPawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Mantle System")
		TEnumAsByte<ECollisionChannel> WalkableSurfaceDetectionChannel = ECC_Visibility;

	UPROPERTY(BlueprintReadOnly, Category = "Mantle System")
		FMantleParams MantleParams;

	UPROPERTY(BlueprintReadOnly, Category = "Mantle System")
		FComponentAndTransform MantleLedgeLS;

	UPROPERTY(BlueprintReadOnly, Category = "Mantle System")
		FTransform MantleTarget = FTransform::Identity;

	UPROPERTY(BlueprintReadOnly, Category = "Mantle System")
		FTransform MantleActualStartOffset = FTransform::Identity;

	UPROPERTY(BlueprintReadOnly, Category = "Mantle System")
		FTransform MantleAnimatedStartOffset = FTransform::Identity;

	/** If a dynamic object has a velocity bigger than this value, do not start mantle */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Mantle System")
		float AcceptableVelocityWhileMantling = 10.0f;

private:
	UPROPERTY()
		ABaseCharacter* OwnerCharacter;

	UPROPERTY()
		UDebugComponent* DebugComponent = nullptr;

	UPROPERTY()
		ABasePlayerController* BasePlayerController = nullptr;
};
