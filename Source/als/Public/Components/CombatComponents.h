#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CombatComponents.generated.h"

UCLASS(Blueprintable, BlueprintType)
class ALS_API UCombatComponents : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UCombatComponents();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

};
