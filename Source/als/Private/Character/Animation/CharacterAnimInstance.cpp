#include "Character/Animation/CharacterAnimInstance.h"
#include "Character/BaseCharacter.h"
#include "Library/MathLibrary.h"
#include "Curves/CurveVector.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

void UCharacterAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	if (IsValid(TryGetPawnOwner()))
	{
		Character = Cast<ABaseCharacter>(TryGetPawnOwner());
	}
}

void UCharacterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (!Character)
	{
		// Fix character looking right on editor
		RotationMode = ERotationMode::VelocityDirection;

		// Don't run in editor
		return;
	}

	if (DeltaSeconds == 0.0f)
	{
		// Prevent update on the first frame (potential division by zero)
		return;
	}

	// Update rest of character information. Others are reflected into anim bp when they're set inside character class
	CharacterInformation.Velocity = Character->GetCharacterMovement()->Velocity;
	CharacterInformation.MovementInput = Character->GetMovementInput();
	CharacterInformation.AimingRotation = Character->GetAimingRotation();
	CharacterInformation.CharacterActorRotation = Character->GetActorRotation();		
}
