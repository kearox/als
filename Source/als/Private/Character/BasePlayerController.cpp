#include "Character/BasePlayerController.h"
#include "Components/DebugComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Character/BaseCharacter.h"

void ABasePlayerController::OnPossess(APawn* NewPawn)
{
	Super::OnPossess(NewPawn);
	PossessedCharacter = Cast<ABaseCharacter>(NewPawn);
	SetupDebugInputs();
}

void ABasePlayerController::BeginPlayingState()
{
	Super::BeginPlayingState();
	if (GetLocalRole() == ROLE_AutonomousProxy)
	{
		SetupDebugInputs();
	}
}

void ABasePlayerController::SetupDebugInputs()
{
	if (PossessedCharacter)
	{
		UActorComponent* Comp = PossessedCharacter->GetComponentByClass(UDebugComponent::StaticClass());
		if (Comp)
		{
			UDebugComponent* DebugComp = Cast<UDebugComponent>(Comp);
			if (InputComponent && DebugComp)
			{
				InputComponent->BindKey(EKeys::Tab, EInputEvent::IE_Pressed, this, &ABasePlayerController::ToggleHud);//TAB
				InputComponent->BindKey(EKeys::V, EInputEvent::IE_Pressed, this, &ABasePlayerController::ToggleDebugView);//V
				InputComponent->BindKey(EKeys::T, EInputEvent::IE_Pressed, this, &ABasePlayerController::ToggleTraces);//T
				InputComponent->BindKey(EKeys::Y, EInputEvent::IE_Pressed, this, &ABasePlayerController::ToggleDebugShapes);//Y
				InputComponent->BindKey(EKeys::U, EInputEvent::IE_Pressed, this, &ABasePlayerController::ToggleLayerColors);//U
				InputComponent->BindKey(EKeys::I, EInputEvent::IE_Pressed, this, &ABasePlayerController::ToggleCharacterInfo);//I
				InputComponent->BindKey(EKeys::Z, EInputEvent::IE_Pressed, this, &ABasePlayerController::ToggleSlomo);//Z
				InputComponent->BindKey(EKeys::M, EInputEvent::IE_Pressed, this, &ABasePlayerController::ToggleDebugMesh);//M
			}
		}
	}
}

